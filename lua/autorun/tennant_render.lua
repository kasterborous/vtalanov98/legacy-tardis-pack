if SERVER then
	hook.Add( "SetupPlayerVisibility", "tennant-Render", function(ply,viewent)
		if IsValid(ply.tennant) then
			AddOriginToPVS(ply.tennant:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "tennant-Render", function()
		rt=GetRenderTarget("tennant_rt",size,size,false)
		mat=Material("models/vtalanov98old/tennant/scanner")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "tennant-Render", function()
		if tobool(GetConVarNumber("tennantint_scanner"))==false then return end
		local tennant=LocalPlayer().tennant
		if IsValid(tennant) and LocalPlayer().tennant_viewmode then
			CamData.origin = tennant:LocalToWorld(Vector(30, 0, 80))
			CamData.angles = tennant:GetAngles()
			LocalPlayer().tennant_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().tennant_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "tennant-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("tennantint_halos"))==false then return end
		local tennant=LocalPlayer().tennant
		if IsValid(tennant) and not LocalPlayer().tennant_render then
			local interior=tennant:GetNWEntity("interior",NULL)
			if IsValid(interior) and interior.parts then
				for k,v in pairs(interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end