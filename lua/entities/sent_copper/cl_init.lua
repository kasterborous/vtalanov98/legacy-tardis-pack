include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw() 
	if not self.phasing and self.visible and not self.invortex then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	elseif self.phasing then
		if self.percent then
			if not self.phasemode and self.highPer <= 0 then
				self.phasing=false
			elseif self.phasemode and self.percent >= 1 then
				self.phasing=false
			end
		end
		
		self.percent = (self.phaselifetime - CurTime())
		self.highPer = self.percent + 0.5
		if self.phasemode then
			self.percent = (1-self.percent)-0.5
			self.highPer = self.percent+0.5
		end
		self.percent = math.Clamp( self.percent, 0, 1 )
		self.highPer = math.Clamp( self.highPer, 0, 1 )

		--Drawing original model
		local normal = self:GetUp()
		local origin = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.highPer ))
		local distance = normal:Dot( origin )
		
		render.EnableClipping( true )
		render.PushCustomClipPlane( normal, distance )
			self:DrawModel()
		render.PopCustomClipPlane()
		
		local restoreT = self:GetMaterial()
		
		--Drawing phase texture
		render.MaterialOverride( self.wiremat )

		normal = self:GetUp()
		distance = normal:Dot( origin )
		render.PushCustomClipPlane( normal, distance )
		
		local normal2 = self:GetUp() * -1
		local origin2 = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.percent ))
		local distance2 = normal2:Dot( origin2 )
		render.PushCustomClipPlane( normal2, distance2 )
			self:DrawModel()
		render.PopCustomClipPlane()
		render.PopCustomClipPlane()
		
		render.MaterialOverride( restoreT )
		render.EnableClipping( false )
	end
end

function ENT:Phase(mode)
	self.phasing=true
	self.phaseactive=true
	self.phaselifetime=CurTime()+1
	self.phasemode=mode
end

function ENT:Initialize()
	self.health=100
	self.phasemode=false
	self.visible=true
	self.flightmode=false
	self.visible=true
	self.power=true
	self.z=0
	self.phasedraw=0
	self.mins = self:OBBMins()
	self.maxs = self:OBBMaxs()
	self.wiremat = Material( "models/vtalanov98old/copper/phase" )
	self.height = self.maxs.z - self.mins.z
end

function ENT:OnRemove()
	if self.flightloop then
		self.flightloop:Stop()
		self.flightloop=nil
	end
	if self.flightloop2 then
		self.flightloop2:Stop()
		self.flightloop2=nil
	end
end

function ENT:Think()
	if tobool(GetConVarNumber("copper_flightsound"))==true then
		if not self.flightloop then
			self.flightloop=CreateSound(self, "vtalanov98old/copper/flight_loop.wav")
			self.flightloop:SetSoundLevel(90)
			self.flightloop:Stop()
		end
		if self.flightmode and self.visible and not self.moving then
			if !self.flightloop:IsPlaying() then
				self.flightloop:Play()
			end
			local e = LocalPlayer():GetViewEntity()
			if !IsValid(e) then e = LocalPlayer() end
			local copper=LocalPlayer().copper
			if not (copper and IsValid(copper) and copper==self and e==LocalPlayer()) then
				local pos = e:GetPos()
				local spos = self:GetPos()
				local doppler = (pos:Distance(spos+e:GetVelocity())-pos:Distance(spos+self:GetVelocity()))/200
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(math.Clamp(r+doppler,80,120),0.1)
				else
					self.flightloop:ChangePitch(math.Clamp((self:GetVelocity():Length()/250)+95+doppler,80,120),0.1)
				end
				self.flightloop:ChangeVolume(GetConVarNumber("copper_flightvol"),0)
			else
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(r,0.1)
				else
					local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
					self.flightloop:ChangePitch(95+p,0.1)
				end
				self.flightloop:ChangeVolume(0.75*GetConVarNumber("copper_flightvol"),0)
			end
		else
			if self.flightloop:IsPlaying() then
				self.flightloop:Stop()
			end
		end
		
		local interior=self:GetNWEntity("interior",NULL)
		if not self.flightloop2 and interior and IsValid(interior) then
			self.flightloop2=CreateSound(interior, "vtalanov98old/copper/flight_loop.wav")
			self.flightloop2:Stop()
		end
		if self.flightloop2 and (self.flightmode or self.invortex) and LocalPlayer().copper_viewmode and not IsValid(LocalPlayer().copper_skycamera) and interior and IsValid(interior) and ((self.invortex and self.moving) or not self.moving) then
			if !self.flightloop2:IsPlaying() then
				self.flightloop2:Play()
				self.flightloop2:ChangeVolume(0.4,0)
			end
			if self.exploded then
				local r=math.random(90,130)
				self.flightloop2:ChangePitch(r,0.1)
			else
				local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
				self.flightloop2:ChangePitch(95+p,0.1)
			end
		elseif self.flightloop2 then
			if self.flightloop2:IsPlaying() then
				self.flightloop2:Stop()
			end
		end
	else
		if self.flightloop then
			self.flightloop:Stop()
			self.flightloop=nil
		end
		if self.flightloop2 then
			self.flightloop2:Stop()
			self.flightloop2=nil
		end
	end
	
	if self.light_on and tobool(GetConVarNumber("copper_dynamiclight"))==true then
		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight.Pos = self:GetPos() + self:GetUp() * 123
			dlight.r = c.r
			dlight.g = c.g
			dlight.b = c.b
			dlight.Brightness = 1
			dlight.Decay = size * 5
			dlight.Size = size
			dlight.DieTime = CurTime() + 1
		end
	end
	if self.health and self.health > 20 and self.visible and self.power and not self.moving and tobool(GetConVarNumber("copper_dynamiclight"))==true then
		local dlight2 = DynamicLight( self:EntIndex() )
		if ( dlight2 ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight2.Pos = self:GetPos() + self:GetUp() * 123
			dlight2.r = c.r
			dlight2.g = c.g
			dlight2.b = c.b
			dlight2.Brightness = 1
			dlight2.Decay = size * 5
			dlight2.Size = size
			dlight2.DieTime = CurTime() + 1
		end
	end
end

net.Receive("copper-UpdateVis", function()
	local ent=net.ReadEntity()
	ent.visible=tobool(net.ReadBit())
end)

net.Receive("copper-Phase", function()
	local copper=net.ReadEntity()
	local interior=net.ReadEntity()
	if IsValid(copper) then
		copper.visible=tobool(net.ReadBit())
		copper:Phase(copper.visible)
		if not copper.visible and tobool(GetConVarNumber("copper_phasesound"))==true then
			copper:EmitSound("vtalanov98old/copper/phase_enable.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("vtalanov98old/copper/phase_enable.wav", 100, 100)
			end
		end
	end
end)

net.Receive("copper-Explode", function()
	local ent=net.ReadEntity()
	ent.exploded=true
end)

net.Receive("copper-UnExplode", function()
	local ent=net.ReadEntity()
	ent.exploded=false
end)

net.Receive("copper-Flightmode", function()
	local ent=net.ReadEntity()
	ent.flightmode=tobool(net.ReadBit())
end)

net.Receive("copper-SetInterior", function()
	local ent=net.ReadEntity()
	ent.interior=net.ReadEntity()
end)

local tpsounds={}
tpsounds[0]={ // normal
	"vtalanov98old/copper/demat.wav",
	"vtalanov98old/copper/mat.wav",
	"vtalanov98old/copper/full.wav"
}
tpsounds[1]={ // fast demat
	"vtalanov98old/copper/fast_demat.wav",
	"vtalanov98old/copper/mat.wav",
	"vtalanov98old/copper/full.wav"
}
tpsounds[2]={ // fast return
	"vtalanov98old/copper/fastreturn_demat.wav",
	"vtalanov98old/copper/fastreturn_mat.wav",
	"vtalanov98old/copper/fastreturn_full.wav"
}
net.Receive("copper-Go", function()
	local copper=net.ReadEntity()
	if IsValid(copper) then
		copper.moving=true
	end
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local long=tobool(net.ReadBit())
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("copper_matsound"))==true then
		if IsValid(copper) and LocalPlayer().copper==copper then
			if copper.visible then
				if long then
					copper:EmitSound(snds[1], 100, pitch)
				else
					copper:EmitSound(snds[3], 100, pitch)
				end
			end
			if interior and IsValid(interior) and LocalPlayer().copper_viewmode and not IsValid(LocalPlayer().copper_skycamera) then
				if long then
				sound.Play("vtalanov98old/copper/demat.wav", interior:LocalToWorld(Vector(0,0,90)))
				else
				sound.Play("vtalanov98old/copper/full.wav", interior:LocalToWorld(Vector(0,0,90)))
				end
			end
		elseif IsValid(copper) and copper.visible then
			local pos=net.ReadVector()
			local pos2=net.ReadVector()
			if pos then
				sound.Play(snds[1], pos, 75, pitch)
			end
			if pos2 and not long then
				sound.Play("vtalanov98old/copper/mat2.wav", pos2, 75, pitch)
			end
		end
	end
end)

net.Receive("copper-Stop", function()
	copper=net.ReadEntity()
	if IsValid(copper) then
		copper.moving=nil
	end
end)

net.Receive("copper-Reappear", function()
	local copper=net.ReadEntity()
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("copper_matsound"))==true then
		if IsValid(copper) and LocalPlayer().copper==copper then
			if copper.visible then
				copper:EmitSound("vtalanov98old/copper/mat2.wav", 100, pitch)
			end
			if interior and IsValid(interior) and LocalPlayer().copper_viewmode and not IsValid(LocalPlayer().copper_skycamera) then
				sound.Play("vtalanov98old/copper/mat.wav", interior:LocalToWorld(Vector(0,0,90)))
			end
		elseif IsValid(copper) and copper.visible then
			sound.Play("vtalanov98old/copper/mat2.wav", net.ReadVector(), 75, pitch)
		end
	end
end)

net.Receive("Player-Setcopper", function()
	local ply=net.ReadEntity()
	ply.copper=net.ReadEntity()
end)

net.Receive("copper-SetHealth", function()
	local copper=net.ReadEntity()
	copper.health=net.ReadFloat()
end)

net.Receive("copper-SetLocked", function()
	local copper=net.ReadEntity()
	local interior=net.ReadEntity()
	local locked=tobool(net.ReadBit())
	local makesound=tobool(net.ReadBit())
	if IsValid(copper) then
		copper.locked=locked
		if tobool(GetConVarNumber("copper_locksound"))==true and makesound then
			sound.Play("vtalanov98old/copper/lock.wav", copper:GetPos())
		end
	end
	if IsValid(interior) then
		if tobool(GetConVarNumber("copper_locksound"))==true and not IsValid(LocalPlayer().copper_skycamera) and makesound then
			sound.Play("vtalanov98old/copper/lock.wav", interior:LocalToWorld(Vector(315,350,-79)))
		end
	end
end)

net.Receive("copper-SetViewmode", function()
	LocalPlayer().copper_viewmode=tobool(net.ReadBit())
	LocalPlayer().ShouldDisableLegs=(not LocalPlayer().copper_viewmode)
	
	if LocalPlayer().copper_viewmode and GetConVarNumber("r_rootlod")>0 then
		Derma_Query("The TARDIS Interior requires model detail on high, set now?", "TARDIS Interior", "Yes", function() RunConsoleCommand("r_rootlod", 0) end, "No", function() end)
	end
		
end)

hook.Add( "ShouldDrawLocalPlayer", "copper-ShouldDrawLocalPlayer", function(ply)
	if IsValid(ply.copper) and not ply.copper_viewmode then
		return false
	end
end)

net.Receive("copper-PlayerEnter", function()
	if tobool(GetConVarNumber("copper_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/copper/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().copper_skycamera) then
			sound.Play("vtalanov98old/copper/door.wav", ent2:LocalToWorld(Vector(315,350,-79)))
		end
	end
end)

net.Receive("copper-PlayerExit", function()
	if tobool(GetConVarNumber("copper_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/copper/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().copper_skycamera) then
			sound.Play("vtalanov98old/copper/door.wav", ent2:LocalToWorld(Vector(315,350,-79)))
		end
	end
end)

net.Receive("copper-SetRepairing", function()
	local copper=net.ReadEntity()
	local repairing=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(copper) then
		copper.repairing=repairing
	end
	if IsValid(interior) and LocalPlayer().copper==copper and LocalPlayer().copper_viewmode and tobool(GetConVarNumber("copperint_powersound"))==true then
		if repairing then
			sound.Play("vtalanov98old/copper/powerdown.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/copper/powerup.wav", interior:GetPos())
		end
	end
end)

net.Receive("copper-BeginRepair", function()
	local copper=net.ReadEntity()
	if IsValid(copper) then
		/*
		local mat=Material("models/vtalanov98old/copper/2010")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/props_combine/metal_combinebridge001")
		end
		*/
	end
end)

net.Receive("copper-FinishRepair", function()
	local copper=net.ReadEntity()
	if IsValid(copper) then
		if tobool(GetConVarNumber("copperint_repairsound"))==true and copper.visible then
			sound.Play("vtalanov98old/copper/repairfinish.wav", copper:GetPos())
		end
		/*
		local mat=Material("models/vtalanov98old/copper/2010")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/vtalanov98old/copper/2010")
		end
		*/
	end
end)

net.Receive("copper-SetLight", function()
	local copper=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(copper) then
		copper.light_on=on
	end
end)

net.Receive("copper-SetPower", function()
	local copper=net.ReadEntity()
	local on=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(copper) then
		copper.power=on
	end
	if IsValid(interior) and LocalPlayer().copper==copper and LocalPlayer().copper_viewmode and tobool(GetConVarNumber("copperint_powersound"))==true then
		if on then
			sound.Play("vtalanov98old/copper/powerup.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/copper/powerdown.wav", interior:GetPos())
		end
	end
end)

net.Receive("copper-SetVortex", function()
	local copper=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(copper) then
		copper.invortex=on
	end
end)

surface.CreateFont( "HUDNumber", {font="Trebuchet MS", size=40, weight=900} )

hook.Add("HUDPaint", "copper-DrawHUD", function()
	local p = LocalPlayer()
	local copper = p.copper
	if copper and IsValid(copper) and copper.health and (tobool(GetConVarNumber("copper_takedamage"))==true or copper.exploded) then
		local health = math.floor(copper.health)
		local n=0
		if health <= 99 then
			n=20
		end
		if health <= 9 then
			n=40
		end
		local col=Color(255,255,255)
		if health <= 20 then
			col=Color(255,0,0)
		end
		draw.RoundedBox( 0, 5, ScrH()-55, 220-n, 50, Color(0, 0, 0, 180) )
		draw.DrawText("Health: "..health.."%","HUDNumber", 15, ScrH()-52, col)
	end
end)

hook.Add("CalcView", "copper_CLView", function( ply, origin, angles, fov )
	local copper=LocalPlayer().copper
	local viewent = LocalPlayer():GetViewEntity()
	if !IsValid(viewent) then viewent = LocalPlayer() end
	local dist= -300
	
	if copper and IsValid(copper) and viewent==LocalPlayer() and not LocalPlayer().copper_viewmode then
		local pos=copper:GetPos()+(copper:GetUp()*50)
		local tracedata={}
		tracedata.start=pos
		tracedata.endpos=pos+ply:GetAimVector():GetNormal()*dist
		tracedata.mask=MASK_NPCWORLDSTATIC
		local trace=util.TraceLine(tracedata)
		local view = {}
		view.origin = trace.HitPos
		view.angles = angles
		return view
	end
end)

hook.Add( "HUDShouldDraw", "copper-HideHUD", function(name)
	local viewmode=LocalPlayer().copper_viewmode
	if ((name == "CHudHealth") or (name == "CHudBattery")) and viewmode then
		return false
	end
end)