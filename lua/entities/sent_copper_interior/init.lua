AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("copper-SetViewmode")
util.AddNetworkString("copperInt-SetParts")
util.AddNetworkString("copperInt-UpdateAdv")
util.AddNetworkString("copperInt-SetAdv")
util.AddNetworkString("copperInt-ControlSound")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/copper/interior.mdl" )
	// cheers to doctor who team for the model
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
	self:DrawShadow(false)
	
	self.phys = self:GetPhysicsObject()
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	
	self:SetNWEntity("copper",self.copper)
	
	self.viewcur=0
	self.throttlecur=0
	self.usecur=0
	self.flightmode=0 //0 is none, 1 is skycamera selection, 2 is idk yet or whatever and so on
	self.step=0
	
	
	if WireLib then
		Wire_CreateInputs(self, { "Demat", "Phase", "Flightmode", "X", "Y", "Z", "XYZ [VECTOR]", "Rot" })
		Wire_CreateOutputs(self, { "Health" })
	end
	
	self:SpawnParts()
	
	if IsValid(self.owner) then
		local rails=tobool(self.owner:GetInfoNum("copperint_rails",1))
		if rails then
			self.rails=self:MakePart("sent_copper_rails", Vector(0,0,0), Angle(0,0,0),true)
		end
		self:SetNWVector("mainlight",Vector(self.owner:GetInfoNum("copperint_mainlight_r",255),self.owner:GetInfoNum("copperint_mainlight_g",50),self.owner:GetInfoNum("copperint_mainlight_b",0)))
		self:SetNWVector("seclight",Vector(self.owner:GetInfoNum("copperint_seclight_r",0),self.owner:GetInfoNum("copperint_seclight_g",255),self.owner:GetInfoNum("copperint_seclight_b",0)))
		self:SetNWVector("warnlight",Vector(self.owner:GetInfoNum("copperint_warnlight_r",200),self.owner:GetInfoNum("copperint_warnlight_g",0),self.owner:GetInfoNum("copperint_warnlight_b",0)))
	end
end

function ENT:SpawnParts()
	if self.parts then
		for k,v in pairs(self.parts) do
			if IsValid(v) then
				v:Remove()
				v=nil
			end
		end
	end
	
	self.parts={}
	
	//chairs
	local vname="Seat_Airboat"
	local chair=list.Get("Vehicles")[vname]
	self.chair1=self:MakeVehicle(self:LocalToWorld(Vector(130,-96,-35)), Angle(0,40,0), chair.Model, chair.Class, vname, chair)
	self.chair2=self:MakeVehicle(self:LocalToWorld(Vector(125,55,-35)), Angle(0,135,0), chair.Model, chair.Class, vname, chair)
	
	//parts	
	self.skycamera=self:MakePart("sent_copper_skycamera", Vector(0,0,-350), Angle(90,0,0),false)
	self.throttle=self:MakePart("sent_copper_throttle", Vector(0,0,0), Angle(0,0,0),true)
	self.atomaccel=self:MakePart("sent_copper_atomaccel", Vector(0,0,0), Angle(0,0,0),true)
	self.screen=self:MakePart("sent_copper_screen", Vector(0,0,0), Angle(0,0,0),true)
	self.flightlever=self:MakePart("sent_copper_flightlever", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.vortex=self:MakePart("sent_copper_vortex", Vector(0,0,0), Angle(0,0,0),true)
	self.directionalpointer=self:MakePart("sent_copper_directionalpointer", Vector(0,0,0), Angle(0,0,0),true)
	self.keyboard=self:MakePart("sent_copper_keyboard", Vector(0,0,0), Angle(0,0,0),true)
	self.helmicregulator=self:MakePart("sent_copper_helmicregulator", Vector(0,0,0), Angle(0,0,0),true)
	self.fastreturn=self:MakePart("sent_copper_fastreturn", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.handbrake=self:MakePart("sent_copper_handbrake", Vector(0,0,0), Angle(0,0,0),true)
	self.longflighttoggle=self:MakePart("sent_copper_longflighttoggle", Vector(0,0, 0), Angle(0,0,0),true)
	self.typewriter=self:MakePart("sent_copper_typewriter", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.physbrake=self:MakePart("sent_copper_physbrake", Vector(0,0,0), Angle(0,0,0),true)
	self.repairlever=self:MakePart("sent_copper_repairlever", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.hads=self:MakePart("sent_copper_hads", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.powerlever=self:MakePart("sent_copper_powerlever", Vector(0,0, 0), Angle(0, 0, 0),true)
	self.isomorphic=self:MakePart("sent_copper_isomorphic", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.phaselever=self:MakePart("sent_copper_phaselever", Vector(0, 0, 0), Angle(0, 0,0),true)
	self.lock=self:MakePart("sent_copper_lock", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.button2=self:MakePart("sent_copper_button2", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.gramophone=self:MakePart("sent_copper_gramophone", Vector(-15,15,30), Angle(0,80,0),true)
	self.door=self:MakePart("sent_copper_door", Vector(335.5,384,-29), Angle(0,81.5,0),true)
	self.unused=self:MakePart("sent_copper_unused", Vector(0,0, 0), Angle(0, 0, 0),true)
	self.levers=self:MakePart("sent_copper_levers", Vector(0,0,0), Angle(0,0,0),true)
	self.rooms=self:MakePart("sent_copper_rooms", Vector(0,0,0), Angle(0,0,0),true)
	self.airpump=self:MakePart("sent_copper_airpump", Vector(0,0,0), Angle(0,0,0),true)
	self.blacklever=self:MakePart("sent_copper_blacklever", Vector(0,0,0), Angle(0,0,0),true)
	self.silverlever=self:MakePart("sent_copper_silverlever", Vector(0,0,0), Angle(0,0,0),true)
	self.silverlever2=self:MakePart("sent_copper_silverlever2", Vector(0,0,0), Angle(0,0,0),true)
	self.bell=self:MakePart("sent_copper_bell", Vector(0,0,0), Angle(0,0,0),true)
	self.blackswitchers=self:MakePart("sent_copper_blackswitchers", Vector(0,0,0), Angle(0,0,0),true)
	self.boringer=self:MakePart("sent_copper_boringer", Vector(0,0,0), Angle(0,0,0),true)
	self.burner=self:MakePart("sent_copper_burner", Vector(0,0,0), Angle(0,0,0),true)
	self.brownlever=self:MakePart("sent_copper_brownlever", Vector(0,0,0), Angle(0,0,0),true)
	self.brownlever2=self:MakePart("sent_copper_brownlever2", Vector(0,0,0), Angle(0,0,0),true)
	self.goldvalve=self:MakePart("sent_copper_goldvalve", Vector(0,0,0), Angle(0,0,0),true)
	self.rotator=self:MakePart("sent_copper_rotator", Vector(0, 0, 0), Angle(0,0,0),true)
	self.rotator2=self:MakePart("sent_copper_rotator2", Vector(0, 0, 0), Angle(0,0,0),true)
	self.rotator3=self:MakePart("sent_copper_rotator3", Vector(0, 0, 0), Angle(0,0,0),true)
	self.rotator4=self:MakePart("sent_copper_rotator4", Vector(0, 0, 0), Angle(0,0,0),true)
	self.rotator5=self:MakePart("sent_copper_rotator5", Vector(0, 0, 0), Angle(0,0,0),true)
	self.safelock=self:MakePart("sent_copper_safelock", Vector(0, 0, 0), Angle(0,0,0),true)
	self.safelock2=self:MakePart("sent_copper_safelock2", Vector(0, 0, 0), Angle(0,0,0),true)
	self.safelock4=self:MakePart("sent_copper_safelock4", Vector(0, 0, 0), Angle(0,0,0),true)
	self.bluevalves=self:MakePart("sent_copper_bluevalves", Vector( 0,0,0), Angle(0,0,0),true)
	self.valves=self:MakePart("sent_copper_valves", Vector(0,0,0), Angle(0,0,0),true)
	self.console=self:MakePart("sent_copper_console", Vector(0,0,0), Angle(0,0,0),true)
	self.pipes=self:MakePart("sent_copper_pipes", Vector(0,0,0), Angle(0,0,0),true)
	self.phone=self:MakePart("sent_copper_phone", Vector(0,0,0), Angle(0,0,0),true)
	self.switchers=self:MakePart("sent_copper_switchers", Vector(0,0,0), Angle(0,0,0),true)
	self.pump=self:MakePart("sent_copper_pump", Vector(0,0,0), Angle(0,0,0),true)
	self.pedal=self:MakePart("sent_copper_pedal", Vector(0,0,0), Angle(0,0,0),true)
	self.pedal2=self:MakePart("sent_copper_pedal2", Vector(-7,0,0), Angle(0,180,0),true)
	self.roundels=self:MakePart("sent_copper_roundels", Vector(0,0,0), Angle(0,0,0),true)
	self.rl=self:MakePart("sent_copper_rl", Vector(0,0,0), Angle(0,0,0),true)
	self.rl2=self:MakePart("sent_copper_rl2", Vector(0,0,0), Angle(0,0,0),true)
	self.curvewalls=self:MakePart("sent_copper_curvewalls", Vector(0,0,0), Angle(0,0,0),true)
	
	timer.Simple(2,function() // delay exists so the entity can register on the client, allows for a ping of just under 2000 (should be fine lol)
		if IsValid(self) and self.parts then
			net.Start("copperInt-SetParts")
				net.WriteEntity(self)
				net.WriteFloat(#self.parts)
				for k,v in pairs(self.parts) do
					net.WriteEntity(v)
				end
			net.Broadcast()
		end
	end)
end

function ENT:StartAdv(mode,ply,pos,ang)
	if self.flightmode==0 and self.step==0 and IsValid(self.copper) and self.copper.power and not self.copper.moving then
		self.flightmode=mode
		self.step=1
		if pos and ang then
			self.advpos=pos
			self.advang=ang
		end
		net.Start("copperInt-SetAdv")
			net.WriteEntity(self)
			net.WriteEntity(ply)
			net.WriteFloat(mode)
		net.Send(ply)
		return true
	else
		return false
	end
end

function ENT:UpdateAdv(ply,success)
	if not (self.flightmode==0) and tobool(GetConVarNumber("copper_advanced"))==true and IsValid(self.copper) and self.copper.power then
		if success then
			self.step=self.step+1
			if self.flightmode==1 and self.step==9 then
				local skycamera=self.skycamera
				if IsValid(self.copper) and not self.copper.moving and IsValid(skycamera) and skycamera.hitpos and skycamera.hitang then
					self.copper:Go(skycamera.hitpos, skycamera.hitang)
					skycamera.hitpos=nil
					skycamera.hitang=nil
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.flightmode=0
				self.step=0
			elseif self.flightmode==2 and self.step==9 then
				if IsValid(self.copper) and not self.copper.moving and self.advpos and self.advpos then
					self.copper:Go(self.advpos, self.advang)
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.advpos=nil
				self.advang=nil
				self.flightmode=0
				self.step=0
			elseif self.flightmode==3 and self.step==9 then
				local success=self.copper:DematFast()
				if not success then
					ply:ChatPrint("Error, may be already teleporting.")
				end
				self.flightmode=0
				self.step=0
			end
		else
			//ply:ChatPrint("Failed.")
			self.flightmode=0
			self.step=0
			self.advpos=nil
			self.advang=nil
		end
		net.Start("copperInt-UpdateAdv")
			net.WriteBit(success)
		net.Send(ply)
	end
end

function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end

function ENT:MakePart(class,vec,ang,weld)
	local ent=ents.Create(class)
	ent.copper=self.copper
	ent.interior=self
	ent.owner=self.owner
	ent:SetPos(self:LocalToWorld(vec))
	ent:SetAngles(ang)
	//ent:SetCollisionGroup(COLLISION_GROUP_WORLD)
	ent:Spawn()
	ent:Activate()
	if weld then
		constraint.Weld(self,ent,0,0)
	end
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	table.insert(self.parts,ent)
	return ent
end

function ENT:MakeVehicle( Pos, Ang, Model, Class, VName, VTable ) // for the chairs
	local ent = ents.Create( Class )
	if (!ent) then return NULL end
	
	ent:SetModel( Model )
	
	-- Fill in the keyvalues if we have them
	if ( VTable && VTable.KeyValues ) then
		for k, v in pairs( VTable.KeyValues ) do
			ent:SetKeyValue( k, v )
		end
	end
		
	ent:SetAngles( Ang )
	ent:SetPos( Pos )
		
	ent:Spawn()
	ent:Activate()
	
	ent.VehicleName 	= VName
	ent.VehicleTable 	= VTable
	
	-- We need to override the class in the case of the Jeep, because it 
	-- actually uses a different class than is reported by GetClass
	ent.ClassOverride 	= Class
	
	ent.copper_part=true
	ent:GetPhysicsObject():EnableMotion(false)
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetColor(Color(255,255,255,0))
	constraint.Weld(self,ent,0,0)
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	
	table.insert(self.parts,ent)

	return ent
end

if WireLib then
	function ENT:TriggerInput(k,v)
		if self.copper and IsValid(self.copper) then
			self.copper:TriggerInput(k,v)
		end
	end
end

function ENT:SetHP(hp)
	if WireLib then
		Wire_TriggerOutput(self, "Health", math.floor(hp))
	end
end

function ENT:Explode()
	self.exploded=true
	
	self.fire = ents.Create("env_fire_trail")
	self.fire:SetPos(self:LocalToWorld(Vector(0,0,0)))
	self.fire:Spawn()
	self.fire:SetParent(self)
	
	local explode = ents.Create("env_explosion")
	explode:SetPos(self:LocalToWorld(Vector(0,0,50)))
	explode:Spawn()
	explode:Fire("Explode",0)
	explode:EmitSound("vtalanov98old/copper/explosion.wav", 100, 100 ) //Adds sound to the explosion
	
	self:SetColor(Color(255,233,200))
end

function ENT:UnExplode()
	self.exploded=false
	
	if self.fire and IsValid(self.fire) then
		self.fire:Remove()
		self.fire=nil
	end
	
	self:SetColor(Color(255,255,255))
end

function ENT:OnRemove()
	if self.fire then
		self.fire:Remove()
		self.fire=nil
	end
	for k,v in pairs(self.parts) do
		if IsValid(v) then
			v:Remove()
			v=nil
		end
	end
end

function ENT:PlayerLookingAt(ply,vec,fov,Width)	
	local Disp = vec - self:WorldToLocal(ply:GetPos()+Vector(0,0,64))
	local Dist = Disp:Length()
	
	local MaxCos = math.abs( math.cos( math.acos( Dist / math.sqrt( Dist * Dist + Width * Width ) ) + fov * ( math.pi / 180 ) ) )
	Disp:Normalize()
	
	if Disp:Dot( ply:EyeAngles():Forward() ) > MaxCos then
		return true
	end
	
    return false
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.copper and IsValid(self.copper) and ply.copper and IsValid(ply.copper) and ply.copper==self.copper and ply.copper_viewmode and not ply.copper_skycamera then

		//this must go last, or bad things may happen
		if CurTime()>self.copper.viewmodecur then
			local pos=Vector(0,0,0)
			local pos2=self:WorldToLocal(ply:GetPos())
			local distance=pos:Distance(pos2)
			if distance < 110 and self:PlayerLookingAt(ply, Vector(0,0,0), 25, 25) then
				self.copper:ToggleViewmode(ply)
				self.usecur=CurTime()+1
				self.copper.viewmodecur=CurTime()+1
				return
			end
		end
	end
end

function ENT:OnTakeDamage(dmginfo)
	if self.copper and IsValid(self.copper) then
		self.copper:OnTakeDamage(dmginfo)
	end
end

function ENT:Think()
	if self.copper and IsValid(self.copper) then
		if self.copper.occupants then
			for k,v in pairs(self.copper.occupants) do
				if self:GetPos():Distance(v:GetPos()) > 2500 and v.copper_viewmode and not v.copper_skycamera then
					self.copper:PlayerExit(v,true)
				end
			end
		end
	end
end