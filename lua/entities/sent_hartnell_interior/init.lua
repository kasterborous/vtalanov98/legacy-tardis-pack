AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("hartnell-SetViewmode")
util.AddNetworkString("hartnellInt-SetParts")
util.AddNetworkString("hartnellInt-UpdateAdv")
util.AddNetworkString("hartnellInt-SetAdv")
util.AddNetworkString("hartnellInt-ControlSound")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/hartnell/interior.mdl" )
	// cheers to doctor who team for the model
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
	self:DrawShadow(false)
	
	self.phys = self:GetPhysicsObject()
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	
	self:SetNWEntity("hartnell",self.hartnell)
	
	self.viewcur=0
	self.throttlecur=0
	self.usecur=0
	self.flightmode=0 //0 is none, 1 is skycamera selection, 2 is idk yet or whatever and so on
	self.step=0
	
	
	if WireLib then
		Wire_CreateInputs(self, { "Demat", "Phase", "Flightmode", "X", "Y", "Z", "XYZ [VECTOR]", "Rot" })
		Wire_CreateOutputs(self, { "Health" })
	end
	
	self:SpawnParts()
	
	if IsValid(self.owner) then
		self:SetNWVector("mainlight",Vector(self.owner:GetInfoNum("hartnellint_mainlight_r",255),self.owner:GetInfoNum("hartnellint_mainlight_g",50),self.owner:GetInfoNum("hartnellint_mainlight_b",0)))
		self:SetNWVector("warnlight",Vector(self.owner:GetInfoNum("hartnellint_warnlight_r",200),self.owner:GetInfoNum("hartnellint_warnlight_g",0),self.owner:GetInfoNum("hartnellint_warnlight_b",0)))
	end
end

function ENT:SpawnParts()
	if self.parts then
		for k,v in pairs(self.parts) do
			if IsValid(v) then
				v:Remove()
				v=nil
			end
		end
	end
	
	self.parts={}
	
	//chairs
	local vname="Seat_Airboat"
	local chair=list.Get("Vehicles")[vname]
	self.chair1=self:MakeVehicle(self:LocalToWorld(Vector(135,-150,20)), Angle(0,20,0), chair.Model, chair.Class, vname, chair)
//	self.chair2=self:MakeVehicle(self:LocalToWorld(Vector(125,55,-30)), Angle(0,135,0), chair.Model, chair.Class, vname, chair)
	
	//parts	
	self.skycamera=self:MakePart("sent_hartnell_skycamera", Vector(0,0,-350), Angle(90,0,0),false)
	self.phaselever=self:MakePart("sent_hartnell_phaselever", Vector(0,0,0), Angle(0,0,0),false)
	self.throttle=self:MakePart("sent_hartnell_throttle", Vector(0,0,0), Angle(0,0,0),true)
//	self.spinmode=self:MakePart("sent_hartnell_spinmode", Vector(0,0,0), Angle(0,0,0),true)
	self.trackingmonitor=self:MakePart("sent_hartnell_trackingmonitor", Vector(0,0,0), Angle(0,0,0),true)
	self.flightlever=self:MakePart("sent_hartnell_flightlever", Vector(0,0,0), Angle(0,0,0),true)
	self.vortex=self:MakePart("sent_hartnell_vortex", Vector(0,0,0), Angle(0,0,0),true)
	self.manualmode=self:MakePart("sent_hartnell_manualmode", Vector(0,0,0), Angle(0,0, 0),true)
	self.helmicregulator=self:MakePart("sent_hartnell_helmic", Vector(0,0,0), Angle(0,0,0),true)
	self.fastreturn=self:MakePart("sent_hartnell_fastreturn", Vector(0,0,0), Angle(0, 0, 0),true)
	self.handbrake=self:MakePart("sent_hartnell_handbrake", Vector(0,0,0), Angle(0,0,0),true)
	self.longflighttoggle=self:MakePart("sent_hartnell_longflighttoggle", Vector(0,0,0), Angle(0, 0, 0),true)
	self.coordinate=self:MakePart("sent_hartnell_coordinate", Vector(0,0,0), Angle(0, 0, 0),true)
	self.physbrake=self:MakePart("sent_hartnell_physbrake", Vector(0,0,0), Angle(0, 0, 0),true)
	self.powerlever=self:MakePart("sent_hartnell_powerlever", Vector(0,0,0), Angle(0, 0, 0),true)
	self.isomorphic=self:MakePart("sent_hartnell_isomorphic", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lock=self:MakePart("sent_hartnell_lock", Vector(0,0,0), Angle(0, 0,0),true)
	self.audiosystem=self:MakePart("sent_hartnell_audiosystem", Vector(0,0,0), Angle(0,0,0),true)
	self.door=self:MakePart("sent_hartnell_door", Vector(338,-24,42), Angle(0,-90,0),true)
	self.hads=self:MakePart("sent_hartnell_hads", Vector(0,0,0), Angle(0, 0, 0),true)
	self.repairlever=self:MakePart("sent_hartnell_repairlever", Vector(0,0,0), Angle(0, 0, 0),true)
	self.console=self:MakePart("sent_hartnell_console", Vector(0,0,0), Angle(0, 0, 0),true)
	self.furniture=self:MakePart("sent_hartnell_furniture", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever2=self:MakePart("sent_hartnell_lever2", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever3=self:MakePart("sent_hartnell_lever3", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever4=self:MakePart("sent_hartnell_lever4", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever5=self:MakePart("sent_hartnell_lever5", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever8=self:MakePart("sent_hartnell_lever8", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever9=self:MakePart("sent_hartnell_lever9", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever13=self:MakePart("sent_hartnell_lever13", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever14=self:MakePart("sent_hartnell_lever14", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever15=self:MakePart("sent_hartnell_lever15", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever16=self:MakePart("sent_hartnell_lever16", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever18=self:MakePart("sent_hartnell_lever18", Vector(0,0,0), Angle(0, 0, 0),true)
	self.lever20=self:MakePart("sent_hartnell_lever20", Vector(0,0,0), Angle(0, 0, 0),true)
	self.toggle1=self:MakePart("sent_hartnell_toggle1", Vector(0,0,0), Angle(0, 0, 0),true)
	self.toggle2=self:MakePart("sent_hartnell_toggle2", Vector(0,0,0), Angle(0, 0, 0),true)
	self.toggle3=self:MakePart("sent_hartnell_toggle3", Vector(0,0,0), Angle(0, 0, 0),true)
	self.tumblers1=self:MakePart("sent_hartnell_tumblers1", Vector(0,0,0), Angle(0, 0, 0),true)

	
	timer.Simple(2,function() // delay exists so the entity can register on the client, allows for a ping of just under 2000 (should be fine lol)
		if IsValid(self) and self.parts then
			net.Start("hartnellInt-SetParts")
				net.WriteEntity(self)
				net.WriteFloat(#self.parts)
				for k,v in pairs(self.parts) do
					net.WriteEntity(v)
				end
			net.Broadcast()
		end
	end)
end

function ENT:StartAdv(mode,ply,pos,ang)
	if self.flightmode==0 and self.step==0 and IsValid(self.hartnell) and self.hartnell.power and not self.hartnell.moving then
		self.flightmode=mode
		self.step=1
		if pos and ang then
			self.advpos=pos
			self.advang=ang
		end
		net.Start("hartnellInt-SetAdv")
			net.WriteEntity(self)
			net.WriteEntity(ply)
			net.WriteFloat(mode)
		net.Send(ply)
		return true
	else
		return false
	end
end

function ENT:UpdateAdv(ply,success)
	if not (self.flightmode==0) and tobool(GetConVarNumber("hartnell_advanced"))==true and IsValid(self.hartnell) and self.hartnell.power then
		if success then
			self.step=self.step+1
			if self.flightmode==1 and self.step==5 then
				local skycamera=self.skycamera
				if IsValid(self.hartnell) and not self.hartnell.moving and IsValid(skycamera) and skycamera.hitpos and skycamera.hitang then
					self.hartnell:Go(skycamera.hitpos, skycamera.hitang)
					skycamera.hitpos=nil
					skycamera.hitang=nil
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.flightmode=0
				self.step=0
			elseif self.flightmode==2 and self.step==5 then
				if IsValid(self.hartnell) and not self.hartnell.moving and self.advpos and self.advpos then
					self.hartnell:Go(self.advpos, self.advang)
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.advpos=nil
				self.advang=nil
				self.flightmode=0
				self.step=0
			elseif self.flightmode==3 and self.step==5 then
				local success=self.hartnell:DematFast()
				if not success then
					ply:ChatPrint("Error, may be already teleporting.")
				end
				self.flightmode=0
				self.step=0
			end
		else
			//ply:ChatPrint("Failed.")
			self.flightmode=0
			self.step=0
			self.advpos=nil
			self.advang=nil
		end
		net.Start("hartnellInt-UpdateAdv")
			net.WriteBit(success)
		net.Send(ply)
	end
end

function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end

function ENT:MakePart(class,vec,ang,weld)
	local ent=ents.Create(class)
	ent.hartnell=self.hartnell
	ent.interior=self
	ent.owner=self.owner
	ent:SetPos(self:LocalToWorld(vec))
	ent:SetAngles(ang)
	//ent:SetCollisionGroup(COLLISION_GROUP_WORLD)
	ent:Spawn()
	ent:Activate()
	if weld then
		constraint.Weld(self,ent,0,0)
	end
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	table.insert(self.parts,ent)
	return ent
end

function ENT:MakeVehicle( Pos, Ang, Model, Class, VName, VTable ) // for the chairs
	local ent = ents.Create( Class )
	if (!ent) then return NULL end
	
	ent:SetModel( Model )
	
	-- Fill in the keyvalues if we have them
	if ( VTable && VTable.KeyValues ) then
		for k, v in pairs( VTable.KeyValues ) do
			ent:SetKeyValue( k, v )
		end
	end
		
	ent:SetAngles( Ang )
	ent:SetPos( Pos )
		
	ent:Spawn()
	ent:Activate()
	
	ent.VehicleName 	= VName
	ent.VehicleTable 	= VTable
	
	-- We need to override the class in the case of the Jeep, because it 
	-- actually uses a different class than is reported by GetClass
	ent.ClassOverride 	= Class
	
	ent.hartnell_part=true
	ent:GetPhysicsObject():EnableMotion(false)
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetColor(Color(255,255,255,0))
	constraint.Weld(self,ent,0,0)
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	
	table.insert(self.parts,ent)

	return ent
end

if WireLib then
	function ENT:TriggerInput(k,v)
		if self.hartnell and IsValid(self.hartnell) then
			self.hartnell:TriggerInput(k,v)
		end
	end
end

function ENT:SetHP(hp)
	if WireLib then
		Wire_TriggerOutput(self, "Health", math.floor(hp))
	end
end

function ENT:Explode()
	self.exploded=true
	
	self.fire = ents.Create("env_fire_trail")
	self.fire:SetPos(self:LocalToWorld(Vector(0,0,0)))
	self.fire:Spawn()
	self.fire:SetParent(self)
	
	local explode = ents.Create("env_explosion")
	explode:SetPos(self:LocalToWorld(Vector(0,0,50)))
	explode:Spawn()
	explode:Fire("Explode",0)
	explode:EmitSound("vtalanov98old/hartnell/explosion.wav", 100, 100 ) //Adds sound to the explosion
	
	self:SetColor(Color(255,233,200))
end

function ENT:UnExplode()
	self.exploded=false
	
	if self.fire and IsValid(self.fire) then
		self.fire:Remove()
		self.fire=nil
	end
	
	self:SetColor(Color(255,255,255))
end

function ENT:OnRemove()
	if self.fire then
		self.fire:Remove()
		self.fire=nil
	end
	for k,v in pairs(self.parts) do
		if IsValid(v) then
			v:Remove()
			v=nil
		end
	end
end

function ENT:PlayerLookingAt(ply,vec,fov,Width)	
	local Disp = vec - self:WorldToLocal(ply:GetPos()+Vector(-47,-23,20))
	local Dist = Disp:Length()
	
	local MaxCos = math.abs( math.cos( math.acos( Dist / math.sqrt( Dist * Dist + Width * Width ) ) + fov * ( math.pi / 180 ) ) )
	Disp:Normalize()
	
	if Disp:Dot( ply:EyeAngles():Forward() ) > MaxCos then
		return true
	end
	
    return false
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.hartnell and IsValid(self.hartnell) and ply.hartnell and IsValid(ply.hartnell) and ply.hartnell==self.hartnell and ply.hartnell_viewmode and not ply.hartnell_skycamera then

		//this must go last, or bad things may happen
		if CurTime()>self.hartnell.viewmodecur then
			local pos=Vector(0,0,0)
			local pos2=self:WorldToLocal(ply:GetPos())
			local distance=pos:Distance(pos2)
			if distance < 130 and self:PlayerLookingAt(ply, Vector(-47,-23,20), 25, 25) then
				self.hartnell:ToggleViewmode(ply)
				self.usecur=CurTime()+1
				self.hartnell.viewmodecur=CurTime()+1
				return
			end
		end
	end
end

function ENT:OnTakeDamage(dmginfo)
	if self.hartnell and IsValid(self.hartnell) then
		self.hartnell:OnTakeDamage(dmginfo)
	end
end

function ENT:Think()
	if self.hartnell and IsValid(self.hartnell) then
		if self.hartnell.occupants then
			for k,v in pairs(self.hartnell.occupants) do
				if self:GetPos():Distance(v:GetPos()) > 800 and v.hartnell_viewmode and not v.hartnell_skycamera then
					self.hartnell:PlayerExit(v,true)
				end
			end
		end
	end
end