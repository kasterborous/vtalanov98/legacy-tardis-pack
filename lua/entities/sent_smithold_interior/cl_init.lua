include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw()
	if LocalPlayer().smithold_viewmode and self:GetNWEntity("smithold",NULL)==LocalPlayer().smithold and not LocalPlayer().smithold_render then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	end
end

function ENT:OnRemove()
	if self.cloisterbell then
		self.cloisterbell:Stop()
		self.cloisterbell=nil
	end
	if self.creaks then
		self.creaks:Stop()
		self.creaks=nil
	end
	if self.idlesound then
		self.idlesound:Stop()
		self.idlesound=nil
	end
end

function ENT:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	self.parts={}
end

net.Receive("smitholdInt-SetParts", function()
	local t={}
	local interior=net.ReadEntity()
	local count=net.ReadFloat()
	for i=1,count do
		local ent=net.ReadEntity()
		ent.smithold_part=true
		if IsValid(interior) then
			table.insert(interior.parts,ent)
		end
	end
end)

net.Receive("smitholdInt-UpdateAdv", function()
	local success=tobool(net.ReadBit())
	if success then
	else
		surface.PlaySound("vtalanov98old/smith/cloister.wav")
	end
end)

net.Receive("smitholdInt-SetAdv", function()
	local interior=net.ReadEntity()
	local ply=net.ReadEntity()
	local mode=net.ReadFloat()
	if IsValid(interior) and IsValid(ply) and mode then
		if ply==LocalPlayer() then
		end
		interior.flightmode=mode
	end
end)

net.Receive("smitholdInt-ControlSound", function()
	local smithold=net.ReadEntity()
	local control=net.ReadEntity()
	local snd=net.ReadString()
	if IsValid(smithold) and IsValid(control) and snd and tobool(GetConVarNumber("smitholdint_controlsound"))==true and LocalPlayer().smithold==smithold and LocalPlayer().smithold_viewmode then
		sound.Play(snd,control:GetPos())
	end
end)

function ENT:Think()
	local smithold=self:GetNWEntity("smithold",NULL)
	if IsValid(smithold) and LocalPlayer().smithold_viewmode and LocalPlayer().smithold==smithold then
		if tobool(GetConVarNumber("smitholdint_cloisterbell"))==true and not IsValid(LocalPlayer().smithold_skycamera) then
			if smithold.health and smithold.health < 21 then
				if self.cloisterbell and !self.cloisterbell:IsPlaying() then
					self.cloisterbell:Play()
				elseif not self.cloisterbell then
					self.cloisterbell = CreateSound(self, "vtalanov98old/smith/cloisterbell_loop.wav")
					self.cloisterbell:Play()
				end
			else
				if self.cloisterbell and self.cloisterbell:IsPlaying() then
					self.cloisterbell:Stop()
					self.cloisterbell=nil
				end
			end
		else
			if self.cloisterbell and self.cloisterbell:IsPlaying() then
				self.cloisterbell:Stop()
				self.cloisterbell=nil
			end
		end

		if tobool(GetConVarNumber("smitholdint_creaks"))==true and not IsValid(LocalPlayer().smithold_skycamera) then
			if not smithold.power or smithold.repairing then
				if self.creaks and !self.creaks:IsPlaying() then
					self.creaks:Play()
				elseif not self.creaks then
					self.creaks = CreateSound(self, "vtalanov98old/smithold/creaks_loop.wav")
					self.creaks:Play()
                                        self.creaks:ChangeVolume(0.4,0)
				end
			else
				if self.creaks and self.creaks:IsPlaying() then
					self.creaks:Stop()
					self.creaks=nil
				end
			end
		else
			if self.creaks and self.creaks:IsPlaying() then
				self.creaks:Stop()
				self.creaks=nil
			end
		end
		
		if tobool(GetConVarNumber("smitholdint_idlesound"))==true and smithold.health and smithold.health >= 1 and not IsValid(LocalPlayer().smithold_skycamera) and not smithold.repairing and smithold.power then
			if self.idlesound and !self.idlesound:IsPlaying() then
				self.idlesound:Play()
			elseif not self.idlesound then
				self.idlesound = CreateSound(self, "vtalanov98old/smith/interior_idle_loop.wav")
				self.idlesound:Play()
				self.idlesound:ChangeVolume(0.7,0)
			end
		else
			if self.idlesound and self.idlesound:IsPlaying() then
				self.idlesound:Stop()
				self.idlesound=nil
			end
		end
		
		if not IsValid(LocalPlayer().smithold_skycamera) and tobool(GetConVarNumber("smitholdint_dynamiclight"))==true then
			if smithold.health and smithold.health > 0 and not smithold.repairing and smithold.power then
				local dlight = DynamicLight( self:EntIndex() )
				if ( dlight ) then
					local size=1024
					local v=self:GetNWVector("mainlight",Vector(0,0,0))
					dlight.Pos = self:LocalToWorld(Vector(0,0,300))
					dlight.r = v.x
					dlight.g = v.y
					dlight.b = v.z
					dlight.Brightness = 3
					dlight.Decay = size * 5
					dlight.Size = size
					dlight.DieTime = CurTime() + 1
				end
			end

			if smithold.health and smithold.health > 0 and not smithold.repairing and smithold.power then
			   local dlight2 = DynamicLight( self:EntIndex()+10000 )
			   if ( dlight2 ) then
				   local size=1024
				   local v=self:GetNWVector("seclight",Vector(0,0,0))
					if smithold.health < 21 then
						v=self:GetNWVector("warnlight",Vector(0,0,0))
					end
				   dlight2.Pos = self:LocalToWorld(Vector(0,0,-50))
				   dlight2.r = v.x
				   dlight2.g = v.y
				   dlight2.b = v.z
				   dlight2.Brightness = 4
				   dlight2.Decay = size * 5
				   dlight2.Size = size
				   dlight2.DieTime = CurTime() + 1
			   end
                        end
			
			if (smithold.moving or smithold.flightmode) then
				if self.timerotor.pos==1 then
					self.timerotor.pos=0
				end
				
				self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.07 )
				self:SetPoseParameter( "glass", self.timerotor.pos )
			end
              end
		
	else
		if self.cloisterbell then
			self.cloisterbell:Stop()
			self.cloisterbell=nil
		end
		if self.creaks then
			self.creaks:Stop()
			self.creaks=nil
		end
		if self.idlesound then
			self.idlesound:Stop()
			self.idlesound=nil
		end
	end
end