AddCSLuaFile( "von.lua" )
AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("copperInt-Locations-GUI")
util.AddNetworkString("copperInt-Locations-Send")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/copper/typewriter.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("copper",self.copper)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.copper) and ((self.copper.isomorphic and not (activator==self.owner)) or not self.copper.power) then
		return
	end
	
	local interior=self.interior
	local copper=self.copper
	if IsValid(interior) and IsValid(self.copper) and IsValid(self) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			if tobool(GetConVarNumber("copper_advanced"))==true then
				interior:UpdateAdv(activator,false)
			end
			net.Start("copperInt-Locations-GUI")
				net.WriteEntity(self.interior)
				net.WriteEntity(self.copper)
				net.WriteEntity(self)
			net.Send(activator)
		end
	end
	if IsValid(self.copper) then
		net.Start("copperInt-ControlSound")
			net.WriteEntity(self.copper)
			net.WriteEntity(self)
			net.WriteString("vtalanov98old/copper/typewriter.wav")
		net.Broadcast()
	end
	
	self:NextThink( CurTime() )
	return true
end

net.Receive("copperInt-Locations-Send", function(l,ply)
	local interior=net.ReadEntity()
	local copper=net.ReadEntity()
	local typewriter=net.ReadEntity()
	if IsValid(interior) and IsValid(copper) and IsValid(typewriter) then
		local pos=Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		local ang=Angle(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		if tobool(GetConVarNumber("copper_advanced"))==true then
			if interior.flightmode==0 and interior.step==0 then
				local success=interior:StartAdv(2,ply,pos,ang)
				if success then
					ply:ChatPrint("Programmable flightmode activated.")
				end
			else
				interior:UpdateAdv(ply,false)
			end
		else
			if not copper.invortex then
				typewriter.pos=pos
				typewriter.ang=ang
				ply:ChatPrint("TARDIS destination set.")
			end
		end
		
		if copper.invortex then
			copper:SetDestination(pos,ang)
			ply:ChatPrint("TARDIS destination set.")
		end
	end
end)