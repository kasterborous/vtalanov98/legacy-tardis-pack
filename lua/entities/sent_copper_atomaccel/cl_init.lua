include('shared.lua')

function ENT:Draw()
	if LocalPlayer().copper==self:GetNWEntity("copper", NULL) and LocalPlayer().copper_viewmode and not LocalPlayer().copper_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	local copper=self:GetNWEntity("copper", NULL)
	if IsValid(copper) and LocalPlayer().copper==copper and LocalPlayer().copper_viewmode then
		local mode=self:GetMode()
		if (copper.flightmode or copper.moving) and not (mode==0) then
			local TargetPos
			if ( mode==-1 ) then
				TargetPos = 1.0
				if self.PosePosition==1 then
					self.PosePosition=0
				end
			elseif ( mode==1 ) then
				TargetPos = 0.0
				if self.PosePosition==0 then
					self.PosePosition=1
				end
			end
			self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1 )
			self:SetPoseParameter( "switch", self.PosePosition )
			self:InvalidateBoneCache()
		end
		
	end
end