include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw() 
	if not self.phasing and self.visible and not self.invortex then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	elseif self.phasing then
		if self.percent then
			if not self.phasemode and self.highPer <= 0 then
				self.phasing=false
			elseif self.phasemode and self.percent >= 1 then
				self.phasing=false
			end
		end
		
		self.percent = (self.phaselifetime - CurTime())
		self.highPer = self.percent + 0.5
		if self.phasemode then
			self.percent = (1-self.percent)-0.5
			self.highPer = self.percent+0.5
		end
		self.percent = math.Clamp( self.percent, 0, 1 )
		self.highPer = math.Clamp( self.highPer, 0, 1 )

		--Drawing original model
		local normal = self:GetUp()
		local origin = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.highPer ))
		local distance = normal:Dot( origin )
		
		render.EnableClipping( true )
		render.PushCustomClipPlane( normal, distance )
			self:DrawModel()
		render.PopCustomClipPlane()
		
		local restoreT = self:GetMaterial()
		
		--Drawing phase texture
		render.MaterialOverride( self.wiremat )

		normal = self:GetUp()
		distance = normal:Dot( origin )
		render.PushCustomClipPlane( normal, distance )
		
		local normal2 = self:GetUp() * -1
		local origin2 = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.percent ))
		local distance2 = normal2:Dot( origin2 )
		render.PushCustomClipPlane( normal2, distance2 )
			self:DrawModel()
		render.PopCustomClipPlane()
		render.PopCustomClipPlane()
		
		render.MaterialOverride( restoreT )
		render.EnableClipping( false )
	end
end

function ENT:Phase(mode)
	self.phasing=true
	self.phaseactive=true
	self.phaselifetime=CurTime()+1
	self.phasemode=mode
end

function ENT:Initialize()
	self.health=100
	self.phasemode=false
	self.visible=true
	self.flightmode=false
	self.visible=true
	self.power=true
	self.z=0
	self.phasedraw=0
	self.mins = self:OBBMins()
	self.maxs = self:OBBMaxs()
	self.wiremat = Material( "models/vtalanov98old/smith/phase" )
	self.height = self.maxs.z - self.mins.z
end

function ENT:OnRemove()
	if self.flightloop then
		self.flightloop:Stop()
		self.flightloop=nil
	end
	if self.flightloop2 then
		self.flightloop2:Stop()
		self.flightloop2=nil
	end
end

function ENT:Think()
	if tobool(GetConVarNumber("smithold_flightsound"))==true then
		if not self.flightloop then
			self.flightloop=CreateSound(self, "vtalanov98old/smith/flight_loop.wav")
			self.flightloop:SetSoundLevel(90)
			self.flightloop:Stop()
		end
		if self.flightmode and self.visible and not self.moving then
			if !self.flightloop:IsPlaying() then
				self.flightloop:Play()
			end
			local e = LocalPlayer():GetViewEntity()
			if !IsValid(e) then e = LocalPlayer() end
			local smithold=LocalPlayer().smithold
			if not (smithold and IsValid(smithold) and smithold==self and e==LocalPlayer()) then
				local pos = e:GetPos()
				local spos = self:GetPos()
				local doppler = (pos:Distance(spos+e:GetVelocity())-pos:Distance(spos+self:GetVelocity()))/200
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(math.Clamp(r+doppler,80,120),0.1)
				else
					self.flightloop:ChangePitch(math.Clamp((self:GetVelocity():Length()/250)+95+doppler,80,120),0.1)
				end
				self.flightloop:ChangeVolume(GetConVarNumber("smithold_flightvol"),0)
			else
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(r,0.1)
				else
					local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
					self.flightloop:ChangePitch(95+p,0.1)
				end
				self.flightloop:ChangeVolume(0.75*GetConVarNumber("smithold_flightvol"),0)
			end
		else
			if self.flightloop:IsPlaying() then
				self.flightloop:Stop()
			end
		end
		
		local interior=self:GetNWEntity("interior",NULL)
		if not self.flightloop2 and interior and IsValid(interior) then
			self.flightloop2=CreateSound(interior, "vtalanov98old/smith/flight_loop_int.wav")
			self.flightloop2:Stop()
		end
		if self.flightloop2 and (self.flightmode or self.invortex) and LocalPlayer().smithold_viewmode and not IsValid(LocalPlayer().smithold_skycamera) and interior and IsValid(interior) and ((self.invortex and self.moving) or not self.moving) then
			if !self.flightloop2:IsPlaying() then
				self.flightloop2:Play()
				self.flightloop2:ChangeVolume(0.4,0)
			end
			if self.exploded then
				local r=math.random(90,130)
				self.flightloop2:ChangePitch(r,0.1)
			else
				local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
				self.flightloop2:ChangePitch(95+p,0.1)
			end
		elseif self.flightloop2 then
			if self.flightloop2:IsPlaying() then
				self.flightloop2:Stop()
			end
		end
	else
		if self.flightloop then
			self.flightloop:Stop()
			self.flightloop=nil
		end
		if self.flightloop2 then
			self.flightloop2:Stop()
			self.flightloop2=nil
		end
	end
	
	if self.light_on and tobool(GetConVarNumber("smithold_dynamiclight"))==true then
		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight.Pos = self:GetPos() + self:GetUp() * 123
			dlight.r = c.r
			dlight.g = c.g
			dlight.b = c.b
			dlight.Brightness = 1
			dlight.Decay = size * 5
			dlight.Size = size
			dlight.DieTime = CurTime() + 1
		end
	end
	if self.health and self.health > 20 and self.visible and self.power and not self.moving and tobool(GetConVarNumber("smithold_dynamiclight"))==true then
		local dlight2 = DynamicLight( self:EntIndex() )
		if ( dlight2 ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight2.Pos = self:GetPos() + self:GetUp() * 123
			dlight2.r = c.r
			dlight2.g = c.g
			dlight2.b = c.b
			dlight2.Brightness = 1
			dlight2.Decay = size * 5
			dlight2.Size = size
			dlight2.DieTime = CurTime() + 1
		end
	end
end

net.Receive("smithold-UpdateVis", function()
	local ent=net.ReadEntity()
	ent.visible=tobool(net.ReadBit())
end)

net.Receive("smithold-Phase", function()
	local smithold=net.ReadEntity()
	local interior=net.ReadEntity()
	if IsValid(smithold) then
		smithold.visible=tobool(net.ReadBit())
		smithold:Phase(smithold.visible)
		if not smithold.visible and tobool(GetConVarNumber("smithold_phasesound"))==true then
			smithold:EmitSound("vtalanov98old/smith/phase_enable.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("vtalanov98old/smith/phase_enable.wav", 100, 100)
			end
		end
	end
end)

net.Receive("smithold-Explode", function()
	local ent=net.ReadEntity()
	ent.exploded=true
end)

net.Receive("smithold-UnExplode", function()
	local ent=net.ReadEntity()
	ent.exploded=false
end)

net.Receive("smithold-Flightmode", function()
	local ent=net.ReadEntity()
	ent.flightmode=tobool(net.ReadBit())
end)

net.Receive("smithold-SetInterior", function()
	local ent=net.ReadEntity()
	ent.interior=net.ReadEntity()
end)

local tpsounds={}
tpsounds[0]={ // normal
	"vtalanov98old/smith/demat.wav",
	"vtalanov98old/smith/mat.wav",
	"vtalanov98old/smith/full.wav"
}
tpsounds[1]={ // fast demat
	"vtalanov98old/smith/fast_demat.wav",
	"vtalanov98old/smith/mat.wav",
	"vtalanov98old/smith/full.wav"
}
tpsounds[2]={ // fast return
	"vtalanov98old/smith/fastreturn_demat_int.wav",
	"vtalanov98old/smith/fastreturn_mat_int.wav",
	"vtalanov98old/smith/fastreturn_full_int.wav"
}
net.Receive("smithold-Go", function()
	local smithold=net.ReadEntity()
	if IsValid(smithold) then
		smithold.moving=true
	end
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local long=tobool(net.ReadBit())
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("smithold_matsound"))==true then
		if IsValid(smithold) and LocalPlayer().smithold==smithold then
			if smithold.visible then
				if long then
					smithold:EmitSound(snds[1], 100, pitch)
				else
					smithold:EmitSound(snds[3], 100, pitch)
				end
			end
			if interior and IsValid(interior) and LocalPlayer().smithold_viewmode and not IsValid(LocalPlayer().smithold_skycamera) then
				if long then
				sound.Play("vtalanov98old/smith/demat_int.wav", interior:LocalToWorld(Vector(0,0,90)))
				else
				sound.Play("vtalanov98old/smith/full_int.wav", interior:LocalToWorld(Vector(0,0,90)))
				end
			end
		elseif IsValid(smithold) and smithold.visible then
			local pos=net.ReadVector()
			local pos2=net.ReadVector()
			if pos then
				sound.Play(snds[1], pos, 75, pitch)
			end
			if pos2 and not long then
				sound.Play("vtalanov98old/smith/mat.wav", pos2, 75, pitch)
			end
		end
	end
end)

net.Receive("smithold-Stop", function()
	smithold=net.ReadEntity()
	if IsValid(smithold) then
		smithold.moving=nil
	end
end)

net.Receive("smithold-Reappear", function()
	local smithold=net.ReadEntity()
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("smithold_matsound"))==true then
		if IsValid(smithold) and LocalPlayer().smithold==smithold then
			if smithold.visible then
				smithold:EmitSound("vtalanov98old/smith/mat.wav", 100, pitch)
			end
			if interior and IsValid(interior) and LocalPlayer().smithold_viewmode and not IsValid(LocalPlayer().smithold_skycamera) then
				sound.Play("vtalanov98old/smith/mat_int.wav", interior:LocalToWorld(Vector(0,0,90)))
			end
		elseif IsValid(smithold) and smithold.visible then
			sound.Play("vtalanov98old/smith/mat.wav", net.ReadVector(), 75, pitch)
		end
	end
end)

net.Receive("Player-Setsmithold", function()
	local ply=net.ReadEntity()
	ply.smithold=net.ReadEntity()
end)

net.Receive("smithold-SetHealth", function()
	local smithold=net.ReadEntity()
	smithold.health=net.ReadFloat()
end)

net.Receive("smithold-SetLocked", function()
	local smithold=net.ReadEntity()
	local interior=net.ReadEntity()
	local locked=tobool(net.ReadBit())
	local makesound=tobool(net.ReadBit())
	if IsValid(smithold) then
		smithold.locked=locked
		if tobool(GetConVarNumber("smithold_locksound"))==true and makesound then
			sound.Play("vtalanov98old/smith/lock.wav", smithold:GetPos())
		end
	end
	if IsValid(interior) then
		if tobool(GetConVarNumber("smithold_locksound"))==true and not IsValid(LocalPlayer().smithold_skycamera) and makesound then
			sound.Play("vtalanov98old/smith/lock.wav", interior:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("smithold-SetViewmode", function()
	LocalPlayer().smithold_viewmode=tobool(net.ReadBit())
	LocalPlayer().ShouldDisableLegs=(not LocalPlayer().smithold_viewmode)
	
	if LocalPlayer().smithold_viewmode and GetConVarNumber("r_rootlod")>0 then
		Derma_Query("The TARDIS Interior requires model detail on high, set now?", "TARDIS Interior", "Yes", function() RunConsoleCommand("r_rootlod", 0) end, "No", function() end)
	end
		
end)

hook.Add( "ShouldDrawLocalPlayer", "smithold-ShouldDrawLocalPlayer", function(ply)
	if IsValid(ply.smithold) and not ply.smithold_viewmode then
		return false
	end
end)

net.Receive("smithold-PlayerEnter", function()
	if tobool(GetConVarNumber("smithold_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/smith/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().smithold_skycamera) then
			sound.Play("vtalanov98old/smith/door.wav", ent2:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("smithold-PlayerExit", function()
	if tobool(GetConVarNumber("smithold_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/smith/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().smithold_skycamera) then
			sound.Play("vtalanov98old/smith/door.wav", ent2:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("smithold-SetRepairing", function()
	local smithold=net.ReadEntity()
	local repairing=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(smithold) then
		smithold.repairing=repairing
	end
	if IsValid(interior) and LocalPlayer().smithold==smithold and LocalPlayer().smithold_viewmode and tobool(GetConVarNumber("smitholdint_powersound"))==true then
		if repairing then
			sound.Play("vtalanov98old/smith/powerdown.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/smith/powerup.wav", interior:GetPos())
		end
	end
end)

net.Receive("smithold-BeginRepair", function()
	local smithold=net.ReadEntity()
	if IsValid(smithold) then
		/*
		local mat=Material("models/vtalanov98old/smithold/2014")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/props_combine/metal_combinebridge001")
		end
		*/
	end
end)

net.Receive("smithold-FinishRepair", function()
	local smithold=net.ReadEntity()
	if IsValid(smithold) then
		if tobool(GetConVarNumber("smitholdint_repairsound"))==true and smithold.visible then
			sound.Play("vtalanov98old/smith/repairfinish.wav", smithold:GetPos())
		end
		/*
		local mat=Material("models/vtalanov98old/smithold/2010")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/vtalanov98old/smithold/2014")
		end
		*/
	end
end)

net.Receive("smithold-SetLight", function()
	local smithold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(smithold) then
		smithold.light_on=on
	end
end)

net.Receive("smithold-SetPower", function()
	local smithold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(smithold) then
		smithold.power=on
	end
	if IsValid(interior) and LocalPlayer().smithold==smithold and LocalPlayer().smithold_viewmode and tobool(GetConVarNumber("smitholdint_powersound"))==true then
		if on then
			sound.Play("vtalanov98old/smith/powerup.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/smith/powerdown.wav", interior:GetPos())
		end
	end
end)

net.Receive("smithold-SetVortex", function()
	local smithold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(smithold) then
		smithold.invortex=on
	end
end)

surface.CreateFont( "HUDNumber", {font="Trebuchet MS", size=40, weight=900} )

hook.Add("HUDPaint", "smithold-DrawHUD", function()
	local p = LocalPlayer()
	local smithold = p.smithold
	if smithold and IsValid(smithold) and smithold.health and (tobool(GetConVarNumber("smithold_takedamage"))==true or smithold.exploded) then
		local health = math.floor(smithold.health)
		local n=0
		if health <= 99 then
			n=20
		end
		if health <= 9 then
			n=40
		end
		local col=Color(255,255,255)
		if health <= 20 then
			col=Color(255,0,0)
		end
		draw.RoundedBox( 0, 5, ScrH()-55, 220-n, 50, Color(0, 0, 0, 180) )
		draw.DrawText("Health: "..health.."%","HUDNumber", 15, ScrH()-52, col)
	end
end)

hook.Add("CalcView", "smithold_CLView", function( ply, origin, angles, fov )
	local smithold=LocalPlayer().smithold
	local viewent = LocalPlayer():GetViewEntity()
	if !IsValid(viewent) then viewent = LocalPlayer() end
	local dist= -300
	
	if smithold and IsValid(smithold) and viewent==LocalPlayer() and not LocalPlayer().smithold_viewmode then
		local pos=smithold:GetPos()+(smithold:GetUp()*50)
		local tracedata={}
		tracedata.start=pos
		tracedata.endpos=pos+ply:GetAimVector():GetNormal()*dist
		tracedata.mask=MASK_NPCWORLDSTATIC
		local trace=util.TraceLine(tracedata)
		local view = {}
		view.origin = trace.HitPos
		view.angles = angles
		return view
	end
end)

hook.Add( "HUDShouldDraw", "smithold-HideHUD", function(name)
	local viewmode=LocalPlayer().smithold_viewmode
	if ((name == "CHudHealth") or (name == "CHudBattery")) and viewmode then
		return false
	end
end)