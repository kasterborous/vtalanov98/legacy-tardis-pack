AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("tennant-SetViewmode")
util.AddNetworkString("tennantInt-SetParts")
util.AddNetworkString("tennantInt-UpdateAdv")
util.AddNetworkString("tennantInt-SetAdv")
util.AddNetworkString("tennantInt-ControlSound")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/tennant/interior.mdl" )
	// cheers to doctor who team for the model
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
	self:DrawShadow(false)
	
	self.phys = self:GetPhysicsObject()
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	
	self:SetNWEntity("tennant",self.tennant)
	
	self.viewcur=0
	self.throttlecur=0
	self.usecur=0
	self.flightmode=0 //0 is none, 1 is skycamera selection, 2 is idk yet or whatever and so on
	self.step=0
	
	
	if WireLib then
		Wire_CreateInputs(self, { "Demat", "Phase", "Flightmode", "X", "Y", "Z", "XYZ [VECTOR]", "Rot" })
		Wire_CreateOutputs(self, { "Health" })
	end
	
	self:SpawnParts()
	
	if IsValid(self.owner) then
		local rails=tobool(self.owner:GetInfoNum("tennantint_rails",1))
		if rails then
			//self.rails=self:MakePart("sent_tennant_rails", Vector(0,0,0), Angle(0,0,0),true)
		end
		self:SetNWVector("mainlight",Vector(self.owner:GetInfoNum("tennantint_mainlight_r",0),self.owner:GetInfoNum("tennantint_mainlight_g",255),self.owner:GetInfoNum("tennantint_mainlight_b",0)))
		self:SetNWVector("seclight",Vector(self.owner:GetInfoNum("tennantint_seclight_r",255),self.owner:GetInfoNum("tennantint_seclight_g",0),self.owner:GetInfoNum("tennantint_seclight_b",0)))
		self:SetNWVector("warnlight",Vector(self.owner:GetInfoNum("tennantint_warnlight_r",200),self.owner:GetInfoNum("tennantint_warnlight_g",0),self.owner:GetInfoNum("tennantint_warnlight_b",0)))
	end
end

function ENT:SpawnParts()
	if self.parts then
		for k,v in pairs(self.parts) do
			if IsValid(v) then
				v:Remove()
				v=nil
			end
		end
	end
	
	self.parts={}
	
	//chairs
	local vname="Seat_Airboat"
	local chair=list.Get("Vehicles")[vname]
	self.chair1=self:MakeVehicle(self:LocalToWorld(Vector(-105,0,45)), Angle(0,-90,0), chair.Model, chair.Class, vname, chair)
	self.chair2=self:MakeVehicle(self:LocalToWorld(Vector(-105,20,45)), Angle(0,-90,0), chair.Model, chair.Class, vname, chair)
	self.chair3=self:MakeVehicle(self:LocalToWorld(Vector(-105,-20,45)), Angle(0,-90,0), chair.Model, chair.Class, vname, chair)
	
	//parts	
	self.skycamera=self:MakePart("sent_tennant_skycamera", Vector(0,0,-350), Angle(90,0,0),false)
	self.throttle=self:MakePart("sent_tennant_throttle", Vector(0,0,0), Angle(0,0,0),true)
//	self.atomaccel=self:MakePart("sent_tennant_atomaccel", Vector(-25,-39,2.5), Angle(0,0,0),true)
	self.screen=self:MakePart("sent_tennant_screen", Vector(0,0,0), Angle(0,-60,0),true)
	self.flightlever=self:MakePart("sent_tennant_flightlever", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.vortex=self:MakePart("sent_tennant_vortex", Vector(0,0,0), Angle(0,0,0),true)
	self.directionalpointer=self:MakePart("sent_tennant_directionalpointer", Vector(0,0,0), Angle(0,0,0),true)
	self.keyboard=self:MakePart("sent_tennant_keyboard", Vector(0,0,0), Angle(0,0,0),true)
	self.helmic=self:MakePart("sent_tennant_helmic", Vector(0,0,0), Angle(0,0,0),true)
	self.fastreturn=self:MakePart("sent_tennant_fastreturn", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.handbrake=self:MakePart("sent_tennant_handbrake", Vector(0,0,0), Angle(0,0,0),true)
	self.longflight=self:MakePart("sent_tennant_longflight", Vector(0,0, 0), Angle(0, 0, 0),true)
	self.coordinate=self:MakePart("sent_tennant_coordinate", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.physbrake=self:MakePart("sent_tennant_physbrake", Vector(0,0,0), Angle(0,180,0),true)
	self.repairlever=self:MakePart("sent_tennant_repairlever", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.hads=self:MakePart("sent_tennant_hads", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.power=self:MakePart("sent_tennant_power", Vector(0,0,0), Angle(0,0,0),true)
	self.isomorphic=self:MakePart("sent_tennant_isomorphic", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.phase=self:MakePart("sent_tennant_phase", Vector(0, 0,0), Angle(0, 0, 0),true)
	self.lock=self:MakePart("sent_tennant_lock", Vector(0, 0, 0), Angle(0, 0, 0),true)
	self.audiosystem=self:MakePart("sent_tennant_audiosystem", Vector(0,0,0), Angle(0,0,0),true)
	self.door=self:MakePart("sent_tennant_door", Vector(314,0,64,7), Angle(0,0,0),true)
	self.levers1=self:MakePart("sent_tennant_levers1", Vector(0,0,0), Angle(0,0,0),true)
	self.levers2=self:MakePart("sent_tennant_levers2", Vector(0,0,0), Angle(0,0,0),true)
	self.levers3=self:MakePart("sent_tennant_levers3", Vector(0,0,0), Angle(0,0,0),true)
	self.levers4=self:MakePart("sent_tennant_levers4", Vector(0,0,0), Angle(0,0,0),true)
	self.levers5=self:MakePart("sent_tennant_levers5", Vector(0,0,0), Angle(0,0,0),true)
	self.levers6=self:MakePart("sent_tennant_levers6", Vector(0,0,0), Angle(0,0,0),true)
	self.bell=self:MakePart("sent_tennant_bell", Vector(0,0,0), Angle(0,0,0),true)
	self.lever1=self:MakePart("sent_tennant_lever1", Vector(0,0,0), Angle(0,0,0),true)
	self.lever3=self:MakePart("sent_tennant_lever3", Vector(0,0,0), Angle(0,0,0),true)
	self.lever4=self:MakePart("sent_tennant_lever4", Vector(0,0,0), Angle(0,0,0),true)
	self.lever5=self:MakePart("sent_tennant_lever5", Vector(0,0,0), Angle(0,0,0),true)
	self.pump1=self:MakePart("sent_tennant_pump1", Vector(0,0,0), Angle(0,0,0),true)
	self.phone=self:MakePart("sent_tennant_phone", Vector(0,0,0), Angle(0,0,0),true)
	self.console=self:MakePart("sent_tennant_console", Vector(0,0,0), Angle(0,0,0),true)
	
	timer.Simple(2,function() // delay exists so the entity can register on the client, allows for a ping of just under 2000 (should be fine lol)
		if IsValid(self) and self.parts then
			net.Start("tennantInt-SetParts")
				net.WriteEntity(self)
				net.WriteFloat(#self.parts)
				for k,v in pairs(self.parts) do
					net.WriteEntity(v)
				end
			net.Broadcast()
		end
	end)
end

function ENT:StartAdv(mode,ply,pos,ang)
	if self.flightmode==0 and self.step==0 and IsValid(self.tennant) and self.tennant.power and not self.tennant.moving then
		self.flightmode=mode
		self.step=1
		if pos and ang then
			self.advpos=pos
			self.advang=ang
		end
		net.Start("tennantInt-SetAdv")
			net.WriteEntity(self)
			net.WriteEntity(ply)
			net.WriteFloat(mode)
		net.Send(ply)
		return true
	else
		return false
	end
end

function ENT:UpdateAdv(ply,success)
	if not (self.flightmode==0) and tobool(GetConVarNumber("tennant_advanced"))==true and IsValid(self.tennant) and self.tennant.power then
		if success then
			self.step=self.step+1
			if self.flightmode==1 and self.step==5 then
				local skycamera=self.skycamera
				if IsValid(self.tennant) and not self.tennant.moving and IsValid(skycamera) and skycamera.hitpos and skycamera.hitang then
					self.tennant:Go(skycamera.hitpos, skycamera.hitang)
					skycamera.hitpos=nil
					skycamera.hitang=nil
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.flightmode=0
				self.step=0
			elseif self.flightmode==2 and self.step==5 then
				if IsValid(self.tennant) and not self.tennant.moving and self.advpos and self.advpos then
					self.tennant:Go(self.advpos, self.advang)
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.advpos=nil
				self.advang=nil
				self.flightmode=0
				self.step=0
			elseif self.flightmode==3 and self.step==5 then
				local success=self.tennant:DematFast()
				if not success then
					ply:ChatPrint("Error, may be already teleporting.")
				end
				self.flightmode=0
				self.step=0
			end
		else
			//ply:ChatPrint("Failed.")
			self.flightmode=0
			self.step=0
			self.advpos=nil
			self.advang=nil
		end
		net.Start("tennantInt-UpdateAdv")
			net.WriteBit(success)
		net.Send(ply)
	end
end

function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end

function ENT:MakePart(class,vec,ang,weld)
	local ent=ents.Create(class)
	ent.tennant=self.tennant
	ent.interior=self
	ent.owner=self.owner
	ent:SetPos(self:LocalToWorld(vec))
	ent:SetAngles(ang)
	//ent:SetCollisionGroup(COLLISION_GROUP_WORLD)
	ent:Spawn()
	ent:Activate()
	if weld then
		constraint.Weld(self,ent,0,0)
	end
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	table.insert(self.parts,ent)
	return ent
end

function ENT:MakeVehicle( Pos, Ang, Model, Class, VName, VTable ) // for the chairs
	local ent = ents.Create( Class )
	if (!ent) then return NULL end
	
	ent:SetModel( Model )
	
	-- Fill in the keyvalues if we have them
	if ( VTable && VTable.KeyValues ) then
		for k, v in pairs( VTable.KeyValues ) do
			ent:SetKeyValue( k, v )
		end
	end
		
	ent:SetAngles( Ang )
	ent:SetPos( Pos )
		
	ent:Spawn()
	ent:Activate()
	
	ent.VehicleName 	= VName
	ent.VehicleTable 	= VTable
	
	-- We need to override the class in the case of the Jeep, because it 
	-- actually uses a different class than is reported by GetClass
	ent.ClassOverride 	= Class
	
	ent.tennant_part=true
	ent:GetPhysicsObject():EnableMotion(false)
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetColor(Color(255,255,255,0))
	constraint.Weld(self,ent,0,0)
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	
	table.insert(self.parts,ent)

	return ent
end

if WireLib then
	function ENT:TriggerInput(k,v)
		if self.tennant and IsValid(self.tennant) then
			self.tennant:TriggerInput(k,v)
		end
	end
end

function ENT:SetHP(hp)
	if WireLib then
		Wire_TriggerOutput(self, "Health", math.floor(hp))
	end
end

function ENT:Explode()
	self.exploded=true
	
	self.fire = ents.Create("env_fire_trail")
	self.fire:SetPos(self:LocalToWorld(Vector(0,0,0)))
	self.fire:Spawn()
	self.fire:SetParent(self)
	
	local explode = ents.Create("env_explosion")
	explode:SetPos(self:LocalToWorld(Vector(0,0,50)))
	explode:Spawn()
	explode:Fire("Explode",0)
	explode:EmitSound("vtalanov98old/tennant/explosion.wav", 100, 100 ) //Adds sound to the explosion
	
	self:SetColor(Color(255,233,200))
end

function ENT:UnExplode()
	self.exploded=false
	
	if self.fire and IsValid(self.fire) then
		self.fire:Remove()
		self.fire=nil
	end
	
	self:SetColor(Color(255,255,255))
end

function ENT:OnRemove()
	if self.fire then
		self.fire:Remove()
		self.fire=nil
	end
	for k,v in pairs(self.parts) do
		if IsValid(v) then
			v:Remove()
			v=nil
		end
	end
end

function ENT:PlayerLookingAt(ply,vec,fov,Width)	
	local Disp = vec - self:WorldToLocal(ply:GetPos()+Vector(0,0,50))
	local Dist = Disp:Length()
	
	local MaxCos = math.abs( math.cos( math.acos( Dist / math.sqrt( Dist * Dist + Width * Width ) ) + fov * ( math.pi / 100 ) ) )
	Disp:Normalize()
	
	if Disp:Dot( ply:EyeAngles():Forward() ) > MaxCos then
		return true
	end
	
    return false
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.tennant and IsValid(self.tennant) and ply.tennant and IsValid(ply.tennant) and ply.tennant==self.tennant and ply.tennant_viewmode and not ply.tennant_skycamera then

		//this must go last, or bad things may happen
		if CurTime()>self.tennant.viewmodecur then
			local pos=Vector(0,0,0)
			local pos2=self:WorldToLocal(ply:GetPos())
			local distance=pos:Distance(pos2)
			if distance < 110 and self:PlayerLookingAt(ply, Vector(0,0,-30), 25, 25) then
				self.tennant:ToggleViewmode(ply)
				self.usecur=CurTime()+1
				self.tennant.viewmodecur=CurTime()+1
				return
			end
		end
	end
end

function ENT:OnTakeDamage(dmginfo)
	if self.tennant and IsValid(self.tennant) then
		self.tennant:OnTakeDamage(dmginfo)
	end
end

function ENT:Think()
        if self.tennant and IsValid(self.tennant) then
		if self.tennant.occupants then
			for k,v in pairs(self.tennant.occupants) do
				if self:GetPos():Distance(v:GetPos()) > 700 and v.tennant_viewmode and not v.tennant_skycamera then
					self.tennant:PlayerExit(v,true)
				end
			end
		end
	end
end