AddCSLuaFile( "von.lua" )
AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("capaldioldInt-Locations-GUI")
util.AddNetworkString("capaldioldInt-Locations-Send")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/capaldi/coordinate.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldiold",self.capaldiold)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.capaldiold) and ((self.capaldiold.isomorphic and not (activator==self.owner)) or not self.capaldiold.power) then
		return
	end
	
	local interior=self.interior
	local capaldiold=self.capaldiold
	if IsValid(interior) and IsValid(self.capaldiold) and IsValid(self) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			if tobool(GetConVarNumber("capaldiold_advanced"))==true then
				interior:UpdateAdv(activator,false)
			end
			net.Start("capaldioldInt-Locations-GUI")
				net.WriteEntity(self.interior)
				net.WriteEntity(self.capaldiold)
				net.WriteEntity(self)
			net.Send(activator)
		end
	end
	if IsValid(self.capaldiold) then
		net.Start("capaldioldInt-ControlSound")
			net.WriteEntity(self.capaldiold)
			net.WriteEntity(self)
			net.WriteString("vtalanov98old/capaldi/coordinate.wav")
		net.Broadcast()
	end
	
	self:NextThink( CurTime() )
	return true
end

net.Receive("capaldioldInt-Locations-Send", function(l,ply)
	local interior=net.ReadEntity()
	local capaldiold=net.ReadEntity()
	local coordinate=net.ReadEntity()
	if IsValid(interior) and IsValid(capaldiold) and IsValid(coordinate) then
		local pos=Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		local ang=Angle(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		if tobool(GetConVarNumber("capaldiold_advanced"))==true then
			if interior.flightmode==0 and interior.step==0 then
				local success=interior:StartAdv(2,ply,pos,ang)
				if success then
					ply:ChatPrint("Programmable flightmode activated.")
				end
			else
				interior:UpdateAdv(ply,false)
			end
		else
			if not capaldiold.invortex then
				coordinate.pos=pos
				coordinate.ang=ang
				ply:ChatPrint("TARDIS destination set.")
			end
		end
		
		if capaldiold.invortex then
			capaldiold:SetDestination(pos,ang)
			ply:ChatPrint("TARDIS destination set.")
		end
	end
end)