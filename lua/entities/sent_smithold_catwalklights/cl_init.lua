include('shared.lua')

function ENT:Draw()
	if LocalPlayer().smithold==self:GetNWEntity("smithold", NULL) and LocalPlayer().smithold_viewmode and not LocalPlayer().smithold_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.catwalklights={}
	self.catwalklights.pos=0
	self.catwalklights.mode=1
	self.PosePosition = 0.5
end

function ENT:Think()
	local smithold=self:GetNWEntity("smithold",NULL)
	if IsValid(smithold) and LocalPlayer().smithold_viewmode and LocalPlayer().smithold==smithold then
	  if LocalPlayer().smithold==self:GetNWEntity("smithold", NULL) and LocalPlayer().smithold_viewmode then
		  local TargetPos = 0.0;
		  if ( self:GetOn() ) then TargetPos = 1.0; end
		  self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1.5 )
		  self:SetPoseParameter( "catwalklights", self.PosePosition )
		  self:InvalidateBoneCache()
		
	  end
			if smithold.health and smithold.health > 0 and not smithold.repairing and smithold.power then
	                          self:SetColor(Color(255,255,255))
                                 if smithold.health < 21 then
                                       self:SetColor(Color(250,0,0))
                                 end
                        else
                                  self:SetColor(Color(100,100,100))
			end
                        mat=Material("models/vtalanov98old/smith/catwalklights")

			if not smithold.moving and not smithold.flightmode then
			          self:SetMaterial("models/vtalanov98old/smith/catwalklightsstatic")
                        else
                                  self:SetMaterial("models/smith/smith/catwalklights")
			end
        end
end