#! /bin/bash

gma_file="legacy_tardis_pack.gma"
addon_id="2670193459"

[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./$gma_file ]] && echo "File $gma_file does not exist! Aborting." && exit 2

gmpublish update -id $addon_id -addon $gma_file -changes "$*"
[[ $? == 0 ]] && rm $gma_file